--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1
-- Dumped by pg_dump version 13.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: cliente; Type: TABLE; Schema: public; Owner: victormasip
--

CREATE TABLE public.cliente (
    id integer NOT NULL,
    nombre text,
    fecha_nacimiento date NOT NULL,
    apellidos character varying(200),
    sexo character(1),
    email character varying(200) NOT NULL,
    telefono character varying(9) NOT NULL,
    dni character varying(9) NOT NULL,
    password character varying(200) NOT NULL,
    imagen bytea
);


ALTER TABLE public.cliente OWNER TO victormasip;

--
-- Name: client_id_seq; Type: SEQUENCE; Schema: public; Owner: victormasip
--

CREATE SEQUENCE public.client_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_id_seq OWNER TO victormasip;

--
-- Name: client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: victormasip
--

ALTER SEQUENCE public.client_id_seq OWNED BY public.cliente.id;


--
-- Name: cuenta; Type: TABLE; Schema: public; Owner: victormasip
--

CREATE TABLE public.cuenta (
    cuenta integer NOT NULL,
    id_cliente integer NOT NULL,
    saldo numeric(10,2),
    creacion date
);


ALTER TABLE public.cuenta OWNER TO victormasip;

--
-- Name: cuenta_id_seq; Type: SEQUENCE; Schema: public; Owner: victormasip
--

CREATE SEQUENCE public.cuenta_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuenta_id_seq OWNER TO victormasip;

--
-- Name: cuenta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: victormasip
--

ALTER SEQUENCE public.cuenta_id_seq OWNED BY public.cuenta.cuenta;


--
-- Name: movimientos; Type: TABLE; Schema: public; Owner: victormasip
--

CREATE TABLE public.movimientos (
    id integer NOT NULL,
    fecha timestamp without time zone,
    cantidad numeric(100,2),
    id_origen integer,
    id_destino integer
);


ALTER TABLE public.movimientos OWNER TO victormasip;

--
-- Name: movimientos_id_seq; Type: SEQUENCE; Schema: public; Owner: victormasip
--

CREATE SEQUENCE public.movimientos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movimientos_id_seq OWNER TO victormasip;

--
-- Name: movimientos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: victormasip
--

ALTER SEQUENCE public.movimientos_id_seq OWNED BY public.movimientos.id;


--
-- Name: cliente id; Type: DEFAULT; Schema: public; Owner: victormasip
--

ALTER TABLE ONLY public.cliente ALTER COLUMN id SET DEFAULT nextval('public.client_id_seq'::regclass);


--
-- Name: cuenta cuenta; Type: DEFAULT; Schema: public; Owner: victormasip
--

ALTER TABLE ONLY public.cuenta ALTER COLUMN cuenta SET DEFAULT nextval('public.cuenta_id_seq'::regclass);


--
-- Name: movimientos id; Type: DEFAULT; Schema: public; Owner: victormasip
--

ALTER TABLE ONLY public.movimientos ALTER COLUMN id SET DEFAULT nextval('public.movimientos_id_seq'::regclass);


--
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: victormasip
--

INSERT INTO public.cliente VALUES (21, 'prova', '2000-02-20', 'prova', 'o', 'prova@gmail.com', '600192010', '1234567K', '$2y$10$abb55fzb45SYMW8z9U5bfeZlZIEgEa1UUX3PWdRMscVOVOOHkNCtK', NULL);


--
-- Data for Name: cuenta; Type: TABLE DATA; Schema: public; Owner: victormasip
--

INSERT INTO public.cuenta VALUES (1, 21, 100.00, '2021-05-25');


--
-- Data for Name: movimientos; Type: TABLE DATA; Schema: public; Owner: victormasip
--



--
-- Name: client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: victormasip
--

SELECT pg_catalog.setval('public.client_id_seq', 21, true);


--
-- Name: cuenta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: victormasip
--

SELECT pg_catalog.setval('public.cuenta_id_seq', 1, true);


--
-- Name: movimientos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: victormasip
--

SELECT pg_catalog.setval('public.movimientos_id_seq', 1, false);


--
-- Name: cliente client_pkey; Type: CONSTRAINT; Schema: public; Owner: victormasip
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);


--
-- Name: cuenta cuenta_pkey; Type: CONSTRAINT; Schema: public; Owner: victormasip
--

ALTER TABLE ONLY public.cuenta
    ADD CONSTRAINT cuenta_pkey PRIMARY KEY (cuenta);


--
-- Name: movimientos movimientos_pkey; Type: CONSTRAINT; Schema: public; Owner: victormasip
--

ALTER TABLE ONLY public.movimientos
    ADD CONSTRAINT movimientos_pkey PRIMARY KEY (id);


--
-- Name: cuenta id_cliente; Type: FK CONSTRAINT; Schema: public; Owner: victormasip
--

ALTER TABLE ONLY public.cuenta
    ADD CONSTRAINT id_cliente FOREIGN KEY (id_cliente) REFERENCES public.cliente(id);


--
-- Name: movimientos id_origen; Type: FK CONSTRAINT; Schema: public; Owner: victormasip
--

ALTER TABLE ONLY public.movimientos
    ADD CONSTRAINT id_origen FOREIGN KEY (id_origen) REFERENCES public.cuenta(cuenta);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: victormasip
--

REVOKE ALL ON SCHEMA public FROM postgres;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO victormasip;
GRANT ALL ON SCHEMA public TO victormasip_production;


--
-- Name: TABLE cliente; Type: ACL; Schema: public; Owner: victormasip
--

GRANT ALL ON TABLE public.cliente TO victormasip_production;


--
-- Name: SEQUENCE client_id_seq; Type: ACL; Schema: public; Owner: victormasip
--

GRANT ALL ON SEQUENCE public.client_id_seq TO victormasip_production;


--
-- Name: TABLE cuenta; Type: ACL; Schema: public; Owner: victormasip
--

GRANT ALL ON TABLE public.cuenta TO victormasip_production;


--
-- Name: SEQUENCE cuenta_id_seq; Type: ACL; Schema: public; Owner: victormasip
--

GRANT ALL ON SEQUENCE public.cuenta_id_seq TO victormasip_production;


--
-- Name: TABLE movimientos; Type: ACL; Schema: public; Owner: victormasip
--

GRANT ALL ON TABLE public.movimientos TO victormasip_production;


--
-- Name: SEQUENCE movimientos_id_seq; Type: ACL; Schema: public; Owner: victormasip
--

GRANT ALL ON SEQUENCE public.movimientos_id_seq TO victormasip_production;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: -; Owner: victormasip_production
--

ALTER DEFAULT PRIVILEGES FOR ROLE victormasip_production GRANT ALL ON SEQUENCES  TO victormasip;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: -; Owner: victormasip
--

ALTER DEFAULT PRIVILEGES FOR ROLE victormasip GRANT ALL ON SEQUENCES  TO victormasip_production;


--
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: -; Owner: victormasip_production
--

ALTER DEFAULT PRIVILEGES FOR ROLE victormasip_production GRANT ALL ON FUNCTIONS  TO victormasip;


--
-- Name: DEFAULT PRIVILEGES FOR FUNCTIONS; Type: DEFAULT ACL; Schema: -; Owner: victormasip
--

ALTER DEFAULT PRIVILEGES FOR ROLE victormasip GRANT ALL ON FUNCTIONS  TO victormasip_production;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: -; Owner: victormasip_production
--

ALTER DEFAULT PRIVILEGES FOR ROLE victormasip_production GRANT ALL ON TABLES  TO victormasip;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: -; Owner: victormasip
--

ALTER DEFAULT PRIVILEGES FOR ROLE victormasip GRANT ALL ON TABLES  TO victormasip_production;


--
-- PostgreSQL database dump complete
--

