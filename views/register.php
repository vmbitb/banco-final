<?php
session_start();
ob_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Registre</title>
    <link href="https://getbootstrap.com/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<?php if (isset($_SESSION['user'])){
    header('Location: login.php');
}else{ ?>
    <body class="text-center d-flex justify-content-center">
        <form class="form-signin w-50 mt-5 pt-5" method="post" action="../controller/controller.php">
            <h1 class="h3 mb-3">Registra un compte</h1>
            <div class="row">

                <div class="col-6 pt-3">
                    <label for="inputName" class="sr-only">Nom</label>
                    <input name="name" placeholder="Nom" type="text"  id="inputName" class="form-control" required>
                </div>

                <div class="col-6 pt-3 pb-3">
                    <label for="inputSurname" class="sr-only">Cognoms</label>
                    <input name="surname" placeholder="Cognoms" type="text" id="inputSurname" class="form-control" />
                </div>

                <div class="col-6">
                    <select id="gender" name="gender" class="form-control">
                        <option value="m">Home</option>
                        <option value="f">Dona</option>
                        <option value="o">Altres</option>
                    </select>
                </div>

                <div class="col-6 pb-3">
                    <label for="inputBirthdate" class="sr-only">Data de naixement</label>
                    <input name="birthdate" type="date" id="inputBirthdate" class="form-control"/>
                </div>

                <div class="col-6">
                    <label for="inputDNI" class="sr-only">DNI</label>
                    <input name="dni" placeholder="DNI" type="text" id="inputDNI" class="form-control" />
                </div>

                <div class="col-6 pb-3">
                    <label for="inputPhoneNumber" class="sr-only">Número de telèfon</label>
                    <input name="phonenumber" placeholder="Número de telèfon" type="text" id="inputPhoneNumber" class="form-control" />
                </div>

                <div class="col-6">
                    <label for="inputEmail" class="sr-only">Correu electrònic</label>
                    <input name="email" placeholder="Correu electrònic" type="text" id="inputEmail" class="form-control"/>
                </div>

                <div class="col-6 pb-5">
                    <label for="inputPassword" class="sr-only">Contrassenya</label>
                    <input name="password" placeholder="Contrassenya" type="password" id="inputPassword" class="form-control"/>
                </div>
            </div>
            
            
            <input name="control" value="register" type="hidden"/>
            <button class="btn btn-lg btn-primary btn-block" name="submit" value="Submit" type="submit"">Registrar</button>
            <?php
                if(isset($_POST['message'])){
                   echo $_POST['message'];
                }
            ?>
            <p class="mt-5 mb-3 text-muted"><a href="login.php">Iniciar sessió</a></p>
        </form>
    </body>
<?php } ?>
</html>