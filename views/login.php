<?php
session_start();
ob_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Login</title>
    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <link href="https://getbootstrap.com/docs/4.5/examples/sign-in/signin.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
</head>
<?php if (isset($_SESSION['user'])){
    header('Location: profile.php');
}else{ ?>
    <body class="text-center">
    <form class="form-signin" method="post" action="../controller/controller.php">
        <h1 class="h3 mb-3">Iniciar sessió</h1>
        <label for="inputName" class="sr-only">DNI</label>
        <div class="p-3"></div>
        <input name="dni" placeholder="DNI" type="text"  id="inputName" class="form-control" required>
        <label for="inputSurname" class="sr-only">Contrassenya</label>
        <div class="p-3"></div>
        <input name="password" placeholder="Contrassenya" type="password" id="inputSurname" class="form-control" />
        <input name="control" value="login" type="hidden"/>
        <button class="btn btn-lg mt-5 mb-5 btn-primary btn-block" name="submit" value="Submit" type="submit"">Enviar</button>
        <?php
        if(isset($_POST['message'])){
            echo $_POST['message'];
        }
        ?>
        <a href="./register.php">Registrar un compte</a>
    </form>
    </body>
<?php } ?>
</html>