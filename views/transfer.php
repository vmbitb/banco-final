<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css" integrity="sha384-DhY6onE6f3zzKbjUPRc2hOzGAdEf4/Dz+WJwBvEYL/lkkIsI3ihufq9hk9K4lVoK" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/js/bootstrap.min.js" integrity="sha384-5h4UG+6GOuV9qXh6HqOLwZMY4mnLPraeTrjT5v07o347pj6IkfuoASuGBhfDsp3d" crossorigin="anonymous"></script>
    <title>
        Transfer
    </title>
</head>

<body>
<style> body { background-color: white !important } a { text-decoration: none };</style>

<div class="p-5">
    <h2>Transfer</h2>
    <a href="profile.php">Perfil | </a>
    <a href="init.php">Els meus comptes | </a>
    <a href="transfer.php">Transfer | </a>
    <a href="query.php">Query | </a>
    <a href="logout.php">Logout</a>
</div>

<h2 class="pl-5">Fer una transferència</h2>
<?php
session_start();
if (isset($_SESSION['saldo']))
    echo $_SESSION['saldo'];
?>
    <form class="p-5" action="../controller/controller.php" method="post" >
        <input name="accountnumber" type="number" placeholder="Número de compte" /><br>
        <input class="mt-3" name="amount" type="number" placeholder="Quantitat"/><br>
        <input class="mt-3" name="comentario" type="text" placeholder="Comentari"/><br>
        <p></p>
        <input name="submit" value="Enviar" type="submit" />
    </form>

    <form class="pl-5" action="controller.php" method="post">
        <select name="cuentas">

            <?php
            require_once('model/CuentaModel.php');
            $accounts=getAccounts('dni');
            for ($i=0; $i<sizeof($accounts) ;$i++){?>
                <option ><?php echo $accounts[$i]["cuenta"] ?></option>
            <?php }?>
        </select>
        <input class="pt-3" name="submit" type="submit" placeholder="Sel·leccionar"/>
        <input name="control" type="hidden" value="query"/>
    </form>

    
</body>
</html>