<html>
<head>
    <title>Query</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css" integrity="sha384-DhY6onE6f3zzKbjUPRc2hOzGAdEf4/Dz+WJwBvEYL/lkkIsI3ihufq9hk9K4lVoK" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/js/bootstrap.min.js" integrity="sha384-5h4UG+6GOuV9qXh6HqOLwZMY4mnLPraeTrjT5v07o347pj6IkfuoASuGBhfDsp3d" crossorigin="anonymous"></script>
</head>
<body>

    <style> body { background-color: white !important } a { text-decoration: none };</style>

    <div class="p-5">
        <h2>Query</h2>
        <a href="profile.php">Perfil | </a>
        <a href="init.php">Els meus comptes | </a>
        <a href="transfer.php">Transfer | </a>
        <a href="query.php">Query | </a>
        <a href="logout.php">Logout</a>
    </div>

    <div class="p-5">
    <h2>Transaccions</h2>
        <p>Llistat de transaccions</p>
        <select id="transsacciones" name="transactionlist" form="transactionform">
            <option value="todas">Totes</option>
            <option value="recibidas">Rebudes</option>
            <option value="enviadas">Enviades</option>
        </select>
    </div>

    <?php
    session_start();
    if (isset($_SESSION['saldo'])) {
        echo "Saldo " . $_SESSION['saldo'] . '<br/>';
    }
    if (isset($_SESSION['lista'])) {
        $movimientos=$_SESSION['lista'];
        echo '<table class="default" rules="all" frame="border">';
        echo '<tr>';
        echo '<th>origen</th>';
        echo '<th>destino</th>';
        echo '<th>hora</th>';
        echo '<th>cantidad</th>';
        echo '</tr>';
        for ($i=0;$i<count($movimientos);$i++){
            echo '<tr>';
            echo '<td>'.$movimientos[$i]['origen'].'</td>';
            echo '<td>'.$movimientos[$i]['destino'].'</td>';
            echo '<td>'.$movimientos[$i]['hora'].'</td>';
            echo '<td>'.$movimientos[$i]['cantidad'].'</td>';
            echo '</tr>';
        }
        echo '</table>';

    }

    ?>
</body>
</html>