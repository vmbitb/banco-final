<?php
require_once('../model/Cliente.php');
require_once('../model/ClienteModel.php');
session_start();
if(isset($_SESSION['user'])){
    $user = unserialize($_SESSION['user']);
}
?>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css" integrity="sha384-DhY6onE6f3zzKbjUPRc2hOzGAdEf4/Dz+WJwBvEYL/lkkIsI3ihufq9hk9K4lVoK" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/js/bootstrap.min.js" integrity="sha384-5h4UG+6GOuV9qXh6HqOLwZMY4mnLPraeTrjT5v07o347pj6IkfuoASuGBhfDsp3d" crossorigin="anonymous"></script>
    <title>
        El teu perfil
    </title>
</head>
<body>
    <style> body { background-color: white !important } a { text-decoration: none };</style>
    <?php if (isset($_SESSION['user'])){ ?>
        <div class="p-5">
            <h2>Perfil</h2>
            <a href="init.php">Els meus comptes | </a>
            <a href="transfer.php">Transfer | </a>
            <a href="query.php">Query | </a>
            <a href="logout.php">Logout</a>
        </div>
        <body style="background-color: antiquewhite;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6 pl-5">
                    <h2>Les teves dades</h2>
                    <p>Nom: <?php echo $user->getNombre();  ?></p>
                    <p>Cognoms: <?php echo $user->getApellidos(); ?></p>
                    <p>Gènere: <?php echo $user->getSexo(); ?></p>
                    <p>Data de naixement: <?php echo $user->getFechaNacimiento(); ?></p>
                    <p>DNI: <?php echo $user->getDni(); ?></p>
                    <p>Número de telèfon: <?php echo $user->getTelefono(); ?></p>
                    <p>Correu electrònic: <?php echo $user->getEmail(); ?></p>
                </div>
                <div class="col-6">
                    <h2>Modificar dades</h2>
                    <form action="../controller/controller.php" method="post" enctype="multipart/form-data">
                        <p>Canviar la imatge de perfil </p>
                        <input type="file" name="upload" value="upload"/>
                        <p>Crear una nova contrassenya:</p>
                        <input name="password" type="password" placeholder="Contrassenya" /><br>
                        <p>Canviar el correu electrònic:</p>
                        <input name="emailuse" type="text" placeholder="Correu" /><br>
                        <p>Canviar el número de telèfon:</p>
                        <input name="telephonenumber" type="number" placeholder="Telèfon"/><br>
                        <input name="register" value="register" type="hidden"/><br>
                        <input name="control" value="profile" type="hidden"/>
                        <input name="submit" value="Guardar canvis" type="submit"/><br>
                    </form>
                </div>
            </div>
        </div>
        </body>


    <?php }else{
        header('Location: login.php');
    } ?>
    </body>
</html>
