<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css" integrity="sha384-DhY6onE6f3zzKbjUPRc2hOzGAdEf4/Dz+WJwBvEYL/lkkIsI3ihufq9hk9K4lVoK" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/js/bootstrap.min.js" integrity="sha384-5h4UG+6GOuV9qXh6HqOLwZMY4mnLPraeTrjT5v07o347pj6IkfuoASuGBhfDsp3d" crossorigin="anonymous"></script>
    <title>
        Upload
    </title>
</head>
<body>
    <style> body { background-color: white !important } a { text-decoration: none };</style>    
    <div class="p-5">
    <h2>Transfer</h2>
        <a href="profile.php">Perfil | </a>
        <a href="init.php">Els meus comptes | </a>
        <a href="transfer.php">Transfer | </a>
        <a href="query.php">Query | </a>
        <a href="logout.php">Logout</a>
    </div>
    <form class="p-5" action="../controller/controller.php" method="post" enctype="multipart/form-data">
        <h3>Select image to upload:</h3>
            <input type="file" name="upload" id="upload">
            <input type="hidden" value="profile" name="control">
            <br>
            <input type="submit" value="Enviar" name="submit">
    </form>
</body>
</html>